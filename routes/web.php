<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home2', 'HomeController@index')->name('home2');


Route::post('/addItem', 'ProductController@create')->name('addItem');
Route::get('/addItem', function () {
    return view('addItem');
})->name('addItem');

Route::get('/productList', 'ProductController@show')->name('productList');

Route::post('/sendProduct', 'InventoryController@create')->name('sendProduct');

Route::get('/inventory', 'InventoryController@show')->name('inventory');
