<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Inventory extends Model
{
    protected $fillable = [
        'item_name', 'amount', 'price_per_item', 'total_price',
    ];

    function insertRow($data,$d){
    	$t = DB::table('inventories')->insert(
            ['item_name' => $data['itemName'], 'amount' => $data['amount'], 'price_per_item' => $data['pitem'],'total_price' => $data['total']]);
    	if($d==1)  DB::table('products')->delete();
    	if($t) return 1;
    	else return 0;
    }
}
