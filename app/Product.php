<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'item_name', 'amount', 'price_per_item', 'total_price',
    ];

    
}
