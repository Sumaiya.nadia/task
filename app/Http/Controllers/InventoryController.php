<?php

namespace App\Http\Controllers;

use App\Inventory;
use Illuminate\Http\Request;
use Session;

class InventoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'itemName' => ['required', 'string', 'max:255'],
            'pitem' => ['required'],
            'amount' => ['required'],
        ]);
    }


    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        /*foreach ($request as $req) {
            $data['itemName'] = $req ->itemName;
            $data['amount'] = $req->amount;
            $data['pitem'] = $req->pitem;
            $data['total'] = $req->total ;

            return Inventory::create([
            
            'item_name' => $data['itemName'],
            'amount' => $data['amount'],
            'price_per_item' =>  $data['pitem'],
            'total_price' => $data['total'],

        ]);
        }
        
*/      $obj = new Inventory(); $limit = $request->c; $c=0;
        for($i=0;$i<$limit;$i+=1){
            $data['itemName'] = $request->itemName[$i];
            $data['amount'] = $request->amount[$i];
            $data['pitem'] = $request->pitem[$i];
            $data['total'] = $request->total[$i] ;
            
            if($i == $limit -1) $d = 1;else $d = 0;
            $is = $obj->insertRow($data,$d);
            if($is)$c++;
        }

        if($c==$limit && $limit != 0) {
            //DB::table('products')->delete();
            Session::flash('message', 'Products sent succesfully!'); 
            Session::flash('alert-class', 'alert-success');  
            return redirect()->route('productList');
        }
        if($limit==0){
            Session::flash('message', 'No item has been added!'); 
            Session::flash('alert-class', 'alert-danger');  
            return redirect()->route('productList');
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function show(Inventory $inventory)
    {
        $list = Inventory::all();

        return view("inventory", ["list"=>$list]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function edit(Inventory $inventory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Inventory $inventory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Inventory $inventory)
    {
        //
    }
}
