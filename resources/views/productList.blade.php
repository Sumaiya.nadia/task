@extends('layouts.sup')

@section('content')
<div class="container">
  <h3>List of products to send</h2>

<table class="table table-striped table-dark table-hover">
    <!-- flash message -->
    <div class="row">
        <div class="col-md-6">
            <div class="flash-message">
                @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                @endif
            </div>
        </div>
        <div class="col-md-6"></div>
    </div>
    <!-- flash message end -->
  <thead>
    <tr>
      <th>Item Name</th>
      <th>Amount</th>
      <th>Price per item</th>
      <th>Total Price</th>
    </tr>
  </thead>
  <tbody>
    <form method="POST" id="my_form" action="{{ route('sendProduct') }}">@csrf
    
    @foreach($list as $row)
    <tr>
      <td><input type="text" name="itemName[]" form="my_form" value ="{{$row->item_name}}" readonly style="color: white; background-color: transparent; border: none;"></td>
      <td><input type="number" step=".01" name="amount[]" form="my_form" value ="{{$row->amount}}" readonly style="color: white; background-color: transparent; border: none;"></td>
      <td><input type="number" step=".01" name="pitem[]" form="my_form" value ="{{$row->price_per_item}}" readonly style="color: white; background-color: transparent; border: none;"></td>
      <td><input type="number" step=".01" name="total[]" form="my_form" value ="{{$row->total_price}}" readonly style="color: white; background-color: transparent; border: none;"></td>
    </tr> 
   
    @endforeach  

  </tbody>
</table>
 <input type="hidden" name="c" value="{{count($list) }}" form="my_form">
 <button class="btn btn-primary" form="my_form">Send</button>
</form>

</div>

@endsection
