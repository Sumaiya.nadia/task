@extends('layouts.app')

@section('content')
<div class="container">
  <h3>Recieved Products</h2>

<table class="table table-striped table-dark table-hover">
    <!-- flash message -->
    <div class="row">
        <div class="col-md-6">
            <div class="flash-message">
                @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                @endif
            </div>
        </div>
        <div class="col-md-6"></div>
    </div>
    <!-- flash message end -->
  <thead>
    <tr>
      <th>Item Name</th>
      <th>Amount</th>
      <th>Price per item</th>
      <th>Total Price</th>
    </tr>
  </thead>
  <tbody>
    
    @foreach($list as $row)
    <tr>
      <td>{{$row->item_name}}</td>
      <td>{{$row->amount}}</td>
      <td>{{$row->price_per_item}}</td>
      <td>{{$row->total_price}}</td>
    </tr> 
   
    @endforeach  

  </tbody>
</table>

</div>

@endsection
